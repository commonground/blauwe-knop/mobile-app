// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/app.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/select_flow.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/repositories/session_process/responses/start_challenge_response.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import 'app_test.dart';

class MockSchuldenClient extends Mock implements AppDebtProcessClient {}

class MockSchuldenRequestRepository extends Mock
    implements SchuldenRequestRepository {}

class MockAppLinkRepository extends Mock implements AppLinkRepository {}

class MockDebtRepository extends Mock implements DebtRepository {}

class MockKeyPairRepository extends Mock implements KeyPairRepository {}

class MockCallBackFunction extends Mock {
  void call();
}

void main() {
  testWidgets('schulden overzicht page', (WidgetTester tester) async {
    var schuldenClient = MockSchuldenClient();

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetPublicKeyPEM()).thenAnswer(
      (_) => Future.value('mock-publickey'),
    );

    var schuldenRequestRepository = MockSchuldenRequestRepository();

    var uniLinkStreamController = StreamController<String>();

    var mockSessionProcessRepository = MockSessionProcessRepository();
    when(mockSessionProcessRepository.startChallenge('', 'mock-publickey'))
        .thenAnswer(
      (_) => Future<StartChallengeResponse>.value(
        StartChallengeResponse(),
      ),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: schuldenClient,
              uniLinkStream: uniLinkStreamController.stream,
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [],
              ),
            ),
          ),
          Provider<FaqRepository>(
            create: (_) => FaqRepository(
              client: FaqMemoryClient(
                faqs: [],
              ),
            ),
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => mockSessionProcessRepository,
          ),
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pump();

    expect(find.byKey(SettingsPageLinkKey), findsNothing);
  });
}
