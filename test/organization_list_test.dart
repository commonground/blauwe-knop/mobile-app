// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/organization_list.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

class MockBlauweKnopClient extends Mock implements AppDebtProcessClient {}

class MockKeyPairRepository extends Mock implements KeyPairRepository {}

abstract class RefreshHandler {
  Future handleOnRefresh();
}

class MockRefreshHandler extends Mock implements RefreshHandler {}

void main() {
  testWidgets('organization list for manual login',
      (WidgetTester tester) async {
    var onePmOnjuneFirst2020 = DateTime.utc(2020, 6, 1, 13, 0);

    var organizations = [
      DebtInformation(
        organization: Organization(
          name: 'first organization',
          apiUrl: '',
          loginUrl: '',
          oin: '00000000000000000000',
          isRegistrator: false,
          registratorUrl: '',
          appLinkProcessUrl: '',
          sessionProcessUrl: '',
        ),
        loggedIn: false,
        schulden: null,
      ),
    ];

    var refreshHandler = MockRefreshHandler();
    when(refreshHandler.handleOnRefresh()).thenAnswer((_) => Future.value());
    await tester.pumpWidget(
      Provider<KeyPairRepository>(
        create: (context) => MockKeyPairRepository(),
        child: MaterialApp(
          theme: Themes.blauweKnop(),
          home: Scaffold(
            body: OrganizationList(
              organizations: organizations,
              loadingTriggeredAt: onePmOnjuneFirst2020,
              onRefresh: refreshHandler.handleOnRefresh,
              isLoading: false,
              onOrganizationTap: null,
              manualLogin: true,
            ),
          ),
        ),
      ),
    );

    expect(find.text('Deelnemende organisaties:'), findsOneWidget);
    expect(find.text('first organization'), findsOneWidget);
    expect(find.text('Geladen op 13:00'), findsOneWidget);
    expect(find.byIcon(Icons.lock), findsOneWidget);

    await tester.drag(find.byType(InkWell), Offset(0.0, 250.0));
    await tester.pumpAndSettle();

    verify(refreshHandler.handleOnRefresh()).called(1);
  });
}
