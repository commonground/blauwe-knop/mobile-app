// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('info toast', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Toast(
        text: 'My text',
        type: ToastType.info,
      ),
    ));

    await tester.pumpAndSettle();

    expect(find.text('My text'), findsOneWidget);
    expect(find.widgetWithIcon(Row, Icons.info), findsOneWidget);
    var container = tester.firstWidget(find.byType(Container)) as Container;
    expect((container.decoration as BoxDecoration).color, Colors.orange[100]);
  });

  testWidgets('error toast', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Toast(
        text: 'My text',
        type: ToastType.error,
      ),
    ));

    await tester.pumpAndSettle();

    expect(find.text('My text'), findsOneWidget);
    expect(find.widgetWithIcon(Row, Icons.error), findsOneWidget);
    var container = tester.firstWidget(find.byType(Container)) as Container;
    expect((container.decoration as BoxDecoration).color, Colors.red[100]);
  });

  testWidgets('success toast', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Toast(
        text: 'My text',
        type: ToastType.success,
      ),
    ));

    await tester.pumpAndSettle();

    expect(find.text('My text'), findsOneWidget);
    expect(find.widgetWithIcon(Row, Icons.check), findsOneWidget);
    var container = tester.firstWidget(find.byType(Container)) as Container;
    expect((container.decoration as BoxDecoration).color, Colors.green[100]);
  });

  testWidgets('toast with action', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: Toast(
        text: 'My text',
        type: ToastType.success,
        actionText: 'My action',
      ),
    ));

    await tester.pumpAndSettle();

    expect(find.text('My text'), findsOneWidget);
    expect(find.text('My action'), findsOneWidget);
  });
}
