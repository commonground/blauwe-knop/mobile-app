## Upload a new build to TestFlight

Please note that you need deployment rights to complete the steps in this guide.
You will use Fastlane to help you build the iOS version of the app and upload the build to TestFlight.
Since you will be building the iOS version you will need to execute the steps of this README on a machine running macOS.

You will need the following tools installed on your macOS machine:

- [Flutter](https://flutter.dev/docs/get-started/install).
- [Xcode](https://developer.apple.com/xcode/).
- [Fastlane](https://docs.fastlane.tools/getting-started/ios/setup/).

Next to the required tools you will need:

- A AppleID that has the `AppManager` role for the `Blauwe Knop` app.
- The password used by [Match](https://docs.fastlane.tools/actions/match/) to decrypt the required certifcate and provisiong profile.
- A Gitlab account which has access to the [app-deployment repository](https://gitlab.com/commonground/blauwe-knop/app-deployment).
- [SSH setup](https://docs.gitlab.com/ee/ssh/) on your Gitlab account.

You will start by configuring the version name for the app. The version name of the app will be the most recent `tag` of the repository.

Verify the tag:

```sh
git describe --tags
```

If the repository does not have the required tag you can add it but make sure the tag is formatted according [semantic versioning](https://semver.org/):

```sh
git tag <version number>
```

After creating the tag make sure push it to the remote so that other developers know the tag is in use:

```sh
git push origin tag <version name>
```

Next, run the deployment script. This will build the app and upload it to TestFlight.

> Open ios folder in Xcode and open android folder in Android Studio, to make sure dependencies and settings are updated.

```sh
FASTLANE_USER=<your-apple-id> sh deploy.sh
```

With this script the APK file will also be generated and published in the folder `build/app/outputs/flutter-apk`.

> This is currently selfsigned with data that can be found in the app-deployment repository. **TODO:** create release pipeline

Fastlane will ask you to log in with your AppleID and it will ask you for a passphrase to decrypt the required certifcate and provisioning profile.

> Ask your teammembers for the passphrase

> Please note that it can take a while for Fastlane to finish.

Your AppleID needs to be invited by an admin of VNG Apple Store Connect.
The necessary roles and rights are:

- Roles
  - App Manager
- Additional Resources
  - Access to Certificates, Identities & Profiles.
    - Access to Cloud Managed Distribution Certificate.
- Apps
  - Schuldinformatie
