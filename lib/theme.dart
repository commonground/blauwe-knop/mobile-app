// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Themes {
  static ThemeData blauweKnop() {
    return ThemeData(
        fontFamily: 'OpenSans',
        primaryColor: Color(0xff0b71a1),
        errorColor: Color(0xfff02b41),
        appBarTheme: AppBarTheme(
          backgroundColor: Color(0xff0b71a1),
        ),
        textTheme: GoogleFonts.sourceSansProTextTheme(
          TextTheme(
            bodyText1: TextStyle(
              color: Color(0xff696969),
              fontSize: 14,
              fontWeight: FontWeight.normal,
            ),
            bodyText2: TextStyle(
              color: Color(0xff212121),
              fontSize: 16,
              fontWeight: FontWeight.normal,
            ),
            headline1: TextStyle(
              color: Color(0xff212121),
              fontSize: 20,
              fontWeight: FontWeight.w600,
              height: 1.3,
            ),
            button: TextStyle(
              fontFamily: 'OpenSans',
              color: Colors.white,
              fontWeight: FontWeight.w400,
              fontSize: 20,
            ),
          ),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xff0b71a1),
          textTheme: ButtonTextTheme.primary,
        ),
        iconTheme: IconThemeData(color: Color(0xff757575), size: 14),
        colorScheme:
            ColorScheme.fromSwatch().copyWith(background: Color(0xffeeeeee)));
  }
}
