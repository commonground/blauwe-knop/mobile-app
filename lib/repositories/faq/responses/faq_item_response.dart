// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

class FaqItemReponse {
  String name;
  AcceptedAnswer acceptedAnswer;

  FaqItemReponse({
    this.name,
    this.acceptedAnswer,
  });

  FaqItemReponse.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    acceptedAnswer = AcceptedAnswer(text: json['acceptedAnswer']['text']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['acceptedAnswer']['text'] = acceptedAnswer.text;
    return data;
  }
}

class AcceptedAnswer {
  String text;

  AcceptedAnswer({this.text});
}
