// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:flutter/material.dart';

class FaqRepository {
  final FaqClient client;

  FaqRepository({
    @required this.client,
  });
}
