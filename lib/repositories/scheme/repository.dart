// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:flutter/material.dart';

class SchemeRepository {
  final SchemeClient client;

  SchemeRepository({
    @required this.client,
  });
}
