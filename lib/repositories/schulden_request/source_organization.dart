// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class SourceOrganization {
  String name;
  String oin;
  String apiBaseUrl;
  String linkToken;

  SourceOrganization({
    this.name,
    this.apiBaseUrl,
    this.oin,
    this.linkToken,
  });

  SourceOrganization.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    apiBaseUrl = json['apiBaseUrl'];
    oin = json['oin'];
    linkToken = json['linkToken'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['apiBaseUrl'] = apiBaseUrl;
    data['oin'] = oin;
    data['linkToken'] = linkToken;
    return data;
  }
}
