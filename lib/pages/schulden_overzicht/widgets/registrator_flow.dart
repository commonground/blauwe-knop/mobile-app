// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/organization_list.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'overview_header.dart';

class RegistratorFlow extends StatelessWidget {
  final Function(BuildContext context) onRefreshHandle;
  final Function(DebtInformation org) onOrganizationTap;
  final VoidCallback onDismissHasSendDebtRequestConfirmation;
  final VoidCallback onStartWizardTap;

  RegistratorFlow({
    @required this.onOrganizationTap,
    @required this.onRefreshHandle,
    @required this.onDismissHasSendDebtRequestConfirmation,
    @required this.onStartWizardTap,
  });

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SchuldenOverzichtBloc, SchuldenOverzichtState>(
      listener: (context, state) {},
      builder: (context, state) {
        return Column(
          children: <Widget>[
            if (state.showToast)
              Toast(
                type: ToastType.success,
                text: 'Uw verzoek is ingediend bij ${state.registratorName}',
                onActionTap: () => onDismissHasSendDebtRequestConfirmation(),
              ),
            if (state.hasUnfinishedDebtRequest != null &&
                state.hasUnfinishedDebtRequest)
              Toast(
                type: ToastType.info,
                text: 'Uw verzoek is nog niet ingediend',
                actionText: 'Ga verder met het verzoek',
                onActionTap: () => onStartWizardTap(),
              ),
            if (state.organizations.isNotEmpty) ...[
              if (state.organizations.any((element) =>
                  element.status is! Expired &&
                  element.status is! NotIncludedInRequest))
                OverviewHeader(state.organizations),
              Expanded(
                child: OrganizationList(
                  organizations: state.organizations,
                  isLoading: state.loading,
                  manualLogin: state.loginPerOrganization,
                  onRefresh: () => onRefreshHandle(context),
                  loadingTriggeredAt: state.loadingTriggeredAt,
                  onOrganizationTap: (org) => onOrganizationTap(org),
                ),
              ),
            ],
          ],
        );
      },
    );
  }
}
