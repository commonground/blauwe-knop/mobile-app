// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/organization_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ManualLoginFlow extends StatelessWidget {
  final Function(BuildContext context) onRefreshHandle;
  final Function(DebtInformation org) onOrganizationTap;

  ManualLoginFlow(
      {@required this.onOrganizationTap, @required this.onRefreshHandle});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SchuldenOverzichtBloc, SchuldenOverzichtState>(
        builder: (context, state) {
      return Column(
        children: <Widget>[
          Expanded(
            child: OrganizationList(
              organizations: state.organizations,
              isLoading: state.loading,
              manualLogin: state.loginPerOrganization,
              onRefresh: () => onRefreshHandle(context),
              loadingTriggeredAt: state.loadingTriggeredAt,
              onOrganizationTap: (org) => onOrganizationTap(org),
            ),
          ),
        ],
      );
    });
  }
}
