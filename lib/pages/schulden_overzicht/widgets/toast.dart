// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:flutter/material.dart';

class Toast extends StatelessWidget {
  final ToastType type;
  final String text;
  final String actionText;
  final VoidCallback onActionTap;

  Toast({
    @required this.type,
    @required this.text,
    this.onActionTap,
    this.actionText,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onActionTap,
      child: Container(
        decoration: BoxDecoration(
          color: type == ToastType.info
              ? Colors.orange[100]
              : type == ToastType.error
                  ? Colors.red[100]
                  : Colors.green[100],
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 4,
              offset: Offset(0, 3),
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              type == ToastType.info
                  ? Icon(Icons.info)
                  : type == ToastType.error
                      ? Icon(
                          Icons.error,
                          color: Colors.red,
                        )
                      : Icon(
                          Icons.check,
                          color: Colors.green,
                        ),
              SizedBox(
                width: 8.0,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 4.0),
                    Text(
                      text,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                            fontSize: 16,
                          ),
                    ),
                    if (actionText != null) ...[
                      SizedBox(height: 8.0),
                      Text(
                        actionText,
                        style: Theme.of(context).textTheme.bodyText2.copyWith(
                              color: Colors.blue,
                              fontSize: 16,
                            ),
                      )
                    ]
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum ToastType { info, success, error }
