// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OverviewHeader extends StatelessWidget {
  final List<DebtInformation> organizations;

  OverviewHeader(this.organizations);

  String _getStatusText(
      int countDebtByOrgReceived, int countDebtRequestsLoading) {
    var statusText = '';
    if (countDebtByOrgReceived > 0 || countDebtRequestsLoading > 0) {
      if (countDebtByOrgReceived > 0) {
        statusText +=
            'Schuldinformatie gecontroleerd bij $countDebtByOrgReceived organisatie';

        if (countDebtByOrgReceived > 1) {
          statusText += 's';
        }
      }

      if (countDebtRequestsLoading > 0) {
        if (countDebtByOrgReceived > 0) {
          statusText += ', n';
        } else {
          statusText += 'N';
        }

        statusText +=
            'og bezig met controleren bij $countDebtRequestsLoading organisatie';

        if (countDebtRequestsLoading > 1) {
          statusText += 's';
        }
      }

      statusText += '.';
    } else {
      statusText += 'Geen schuldinformatie beschikbaar.';
    }

    return statusText;
  }

  @override
  Widget build(BuildContext context) {
    var showStatusIcon = false;

    var countDebtByOrgReceived =
        organizations.where((element) => element.status is ReceivedDebt).length;

    var countDebtRequestsLoading = organizations
        .where((element) => element.status is! DebtRequestEndStatus)
        .length;

    if (countDebtRequestsLoading > 0 && countDebtByOrgReceived == 0) {
      showStatusIcon = true;
    }

    return Header(
      <Widget>[
        Text(
          _getStatusText(countDebtByOrgReceived, countDebtRequestsLoading),
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
      icon: showStatusIcon
          ? FaIcon(
              FontAwesomeIcons.clock,
              size: Theme.of(context).iconTheme.size,
              color: Theme.of(context).textTheme.bodyText1.color,
            )
          : null,
    );
  }
}
