// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:blauwe_knop/pages/faq/bloc/faqs_bloc.dart';
import 'package:blauwe_knop/pages/faq/widgets/faq_widget.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class FaqsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var faqRepository = Provider.of<FaqRepository>(context);

    return Scaffold(
      appBar: AppBar(
          title: Text(
            'Veelgestelde vragen',
          ),
          leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 38,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )),
      body: BlocProvider<FaqsBloc>(
        create: (context) {
          return FaqsBloc(
            faqRepository,
          );
        },
        child: BlocBuilder<FaqsBloc, FaqsState>(
          builder: (context, state) {
            final bloc = BlocProvider.of<FaqsBloc>(context);
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                child: FaqWidget(
                  faqItems: state.faqItems,
                  onExpandTap: (int index, bool isExpanded) {
                    bloc.add(FaqItemExpand(index, isExpanded));
                  },
                  backgroundColor: Colors.white,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
