// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:blauwe_knop/pages/faq/bloc/faqs_bloc.dart';
import 'package:flutter/material.dart';

class FaqWidget extends StatelessWidget {
  final List<FaqItem> faqItems;
  final void Function(int, bool) onExpandTap;
  final Color backgroundColor;

  FaqWidget({
    @required this.faqItems,
    @required this.onExpandTap,
    @required this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: (faqItems != null)
          ? _buildPanel(context)
          : Center(child: CircularProgressIndicator()),
    );
  }

  Widget _buildPanel(BuildContext context) {
    var expansionPanels = <ExpansionPanel>[];
    expansionPanels = faqItems.map<ExpansionPanel>(
      (FaqItem item) {
        return ExpansionPanel(
          backgroundColor: backgroundColor,
          canTapOnHeader: true,
          isExpanded: item.isExpanded,
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: Text(item.question),
            );
          },
          body: ListTile(
            title: Text(item.answer),
          ),
        );
      },
    ).toList();
    return ExpansionPanelList(
      expansionCallback: onExpandTap,
      children: expansionPanels,
      elevation: 0,
    );
  }
}
