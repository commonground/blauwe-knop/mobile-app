// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/authentication-wizard/bloc/wizard_bloc.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/all_or_manual_selection.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/confirm_request.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/manual_organization_selection.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/select_aggregator.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/universal_link/repository.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class AuthenticationWizardPage extends StatefulWidget {
  final Function onWizardCompletedHandler;

  AuthenticationWizardPage({
    @required this.onWizardCompletedHandler,
  }) : super();

  @override
  _AuthenticationWizardPageState createState() =>
      _AuthenticationWizardPageState();
}

class _AuthenticationWizardPageState extends State<AuthenticationWizardPage> {
  Function onWizardCompletedHandler;

  @override
  Widget build(BuildContext context) {
    var schemeRepository = Provider.of<SchemeRepository>(context);
    var schuldenRequestRepository =
        Provider.of<SchuldenRequestRepository>(context);
    var callbackRepository = Provider.of<CallBackRepository>(context);
    var appDebtProcessRepository =
        Provider.of<AppDebtProcessRepository>(context);
    var keyPairRepository = Provider.of<KeyPairRepository>(context);
    return BlocProvider<WizardBloc>(
      create: (BuildContext context) {
        var bloc = WizardBloc(
          schemeRepository,
          schuldenRequestRepository,
          widget.onWizardCompletedHandler,
          callbackRepository,
          appDebtProcessRepository,
          keyPairRepository,
        );

        bloc.add(LoadUnfinishedRequest());
        bloc.add(LoadOrganizations());
        return bloc;
      },
      child: BlocBuilder<WizardBloc, WizardState>(
        builder: (context, _) {
          final bloc = BlocProvider.of<WizardBloc>(context);
          return ProvidedAuthenticationWizardPage(bloc: bloc);
        },
      ),
    );
  }
}

class ProvidedAuthenticationWizardPage extends StatefulWidget {
  final WizardBloc bloc;

  const ProvidedAuthenticationWizardPage({@required this.bloc}) : super();

  @override
  ProvidedAuthenticationWizardPageState createState() =>
      ProvidedAuthenticationWizardPageState(bloc: bloc);
}

class ProvidedAuthenticationWizardPageState
    extends State<ProvidedAuthenticationWizardPage> {
  final WizardBloc bloc;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  ProvidedAuthenticationWizardPageState({@required this.bloc}) : super();

  Map<String, WidgetBuilder> _routeBuilders() {
    return {
      AllOrManualSelection.routeName: (_) => AllOrManualSelection(
            submitUseAllOrganizationsHandler: _submitUseAllOrganizations,
            onBackTapped: () => _closeWizard(),
          ),
    };
  }

  Widget _stepToNewPage(int step) {
    switch (step) {
      case 1:
        return AllOrManualSelection(
          submitUseAllOrganizationsHandler: _submitUseAllOrganizations,
          onBackTapped: () => _closeWizard(),
        );

      case 2:
        return ManualOrganizationSelection(
          submitSelectedOrganizations: _organizationsSelected,
          onBackTapped: _onBackTapped,
        );

      case 3:
        return SelectAggregator(
          submitSelectedAggregator: _aggregatorSelected,
          onBackTapped: _onBackTapped,
        );

      case 4:
        return ConfirmRequest(
          submitRequest: _submitRequest,
          onBackTapped: _onBackTapped,
        );

      default:
        debugPrint('unknown step $step. not sure which route to display.');
        return null;
    }
  }

  void _onBackTapped() {
    bloc.add(NavigateToPreviousStep());
  }

  void _closeWizard() {
    Navigator.of(context).pop();
  }

  void _submitUseAllOrganizations(bool useAllOrganizations) {
    if (!useAllOrganizations) {
      bloc.add(UseManualSelection());
    } else {
      bloc.add(SelectAllOrganizations());
    }
  }

  void _organizationsSelected(List<Organization> selectedOrganizations) {
    bloc.add(
      OrganizationsSelected(
        organizations: selectedOrganizations,
      ),
    );
  }

  void _aggregatorSelected(Organization aggregator) {
    bloc.add(
      AggregatorSelected(
        aggregator: aggregator,
      ),
    );
  }

  void _submitRequest() {
    bloc.add(SubmitRequest());
  }

  Route _createPushRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(
          begin: begin,
          end: end,
        ).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  Route _createPopRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(-1.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(
          begin: begin,
          end: end,
        ).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders();
    return BlocListener<WizardBloc, WizardState>(
      listenWhen: (WizardState previousState, WizardState state) {
        return previousState.currentStep != state.currentStep;
      },
      listener: (BuildContext context, WizardState state) {
        if (state.currentStep > state.comingFromStep) {
          navigatorKey.currentState.pushReplacement(
            _createPushRoute(
              _stepToNewPage(state.currentStep),
            ),
          );
        } else if (state.currentStep < state.comingFromStep) {
          navigatorKey.currentState.pushReplacement(
            _createPopRoute(
              _stepToNewPage(state.currentStep),
            ),
          );
        }
      },
      child: Navigator(
        key: navigatorKey,
        initialRoute: AllOrManualSelection.routeName,
        onGenerateRoute: (RouteSettings settings) {
          if (!routeBuilders.containsKey(settings.name)) {
            throw Exception('Invalid route: ${settings.name}');
          }
          final child = routeBuilders[settings.name];
          return MaterialPageRoute(builder: child, settings: settings);
        },
      ),
    );
  }
}
