import 'package:flutter/material.dart';

class CustomBackButton extends StatelessWidget {
  final VoidCallback onPressed;

  CustomBackButton({@required this.onPressed});
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.chevron_left,
        size: 36,
      ),
      onPressed: () => onPressed(),
    );
  }
}
