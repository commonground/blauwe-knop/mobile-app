// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:flutter/foundation.dart';

class Faq {
  String question;
  String answer;

  Faq({
    @required this.question,
    @required this.answer,
  });
}
