// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/app.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/repositories/universal_link/repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<AppDebtProcessRepository>(
          create: (_) => AppDebtProcessRepository(
            client: SchuldenHTTPClient(),
            uniLinkStream: linkStream,
          ),
        ),
        Provider<DebtRepository>(
          create: (_) => LocalDebtRepository(),
        ),
        Provider<AuthorizationRepository>(
          create: (_) => AuthorizationRepository(),
        ),
        Provider<SchemeRepository>(
          create: (_) => SchemeRepository(
            client: SchemeHTTPClient(
              baseUrl:
                  'https://gitlab.com/commonground/blauwe-knop/scheme/-/raw/master/organizations.json',
            ),
          ),
        ),
        Provider<FaqRepository>(
          create: (_) => FaqRepository(
            client: FaqHTTPClient(
              baseUrl:
                  'https://gitlab.com/commonground/blauwe-knop/mobile-app/-/raw/master/assets/faqs.json',
            ),
          ),
        ),
        Provider<AppLinkRepository>(
          create: (_) => AppLinkHTTPRepository(),
        ),
        Provider<SchuldenRequestRepository>(
          create: (_) => SchuldenRequestSecureStorage(),
        ),
        Provider<CallBackRepository>(
          create: (_) => UniLinkRepository(),
        ),
        Provider<KeyPairRepository>(create: (_) {
          var localKeyPairRepository = LocalKeyPairRepository();
          localKeyPairRepository.UpdateKeyPairStream();
          return localKeyPairRepository;
        }),
        Provider<SessionProcessRepository>(
          create: (_) => SessionProcessHTTPRepository(),
        )
      ],
      child: BlauweKnopApp(),
    ),
  );
}
